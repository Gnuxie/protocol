## gnuxie.protocol

What is this?

Basic protocol introspection.

```
(define-protocol exit-point-entry ()
    ((symbol :initarg :symbol)
     (value :initarg :value :writer t))
  (defgeneric invalidate (exit-point-entry))
  (defgeneric valid-p (exit-point-entry)))
```


this makes an entry in (assuming local nickname `protocol` for `gnuxie.protocol`)
protocol:find-protocol using some classes in `code/protocol.lisp` you can
use to get the meaty information from.

The only downside is that have not bothered to implement protocol inheritance
yet because that requires a distinction between direct and effective protocol traits.
Actually i should figure that out before anyone uses it aaaaa.

See `code/example.lisp` for an example.
