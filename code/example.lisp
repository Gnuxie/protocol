#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.protocol)

;;; http://metamodular.com/protocol.pdf
;;; http://metamodular.com/SICL/sicl-specification.pdf 180
;;; base of CLHS + metamodular protocol.pdf for extra info
;;; to use in defgeneric intercept macros etc.

;;; we also want a way to refer to other protocol entries or classes
;;; in the documentation, not sure if shinmera/staple does this automatically
;;; or not, but if not find a way to do it.

;;; the see also stuff can be extra initargs i guess in the defgeneric macro etc.
(define-protocol exit-point-entry ()
    ((symbol :initarg :symbol)
     (value :initarg :value :writer t))
  (defgeneric invalidate (exit-point-entry))
  (defgeneric valid-p (exit-point-entry)))
