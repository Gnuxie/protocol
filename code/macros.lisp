#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.protocol)

(defun transform-place-description (placed)
  ;; naa cos we allow multiple writers and bs like that.
  ;; use t for same as name.
  (destructuring-bind (name &rest rest) placed
    (let ((initargs '())
          (writers '())
          (lambda-list (list name)))
      (a:doplist
          (key value rest)
          (ecase key
            (:initarg (push value initargs))
            (:writer  (if (eql t value)
                          (push name writers)
                          (push value writers)))
            (:lambda-list
             (setf lambda-list value))))
      `(make-instance 'protocol-place
                      :writers ',writers
                      :initargs ',initargs
                      :lambda-list ',lambda-list
                      :name ',name))))

(defun items<-define-protocol-body (body)
  (loop for (macro-name name &rest do-not-care)
          in body
        do (assert (eql 'defgeneric macro-name))
        collect `(make-instance 'protocol-generic-function
                               :name ',name)))

(defmacro define-protocol
    (name (&rest super-protocols) (&rest places) &body body)
  (unless (null super-protocols)
    (error "Haven't implemented super-protocols
yet. It requires a distinction between direct and effective protocol items.
And compute-effective-items semantics and the rest of it, which
is a major pain in the ass."))
  `(progn
     (defclass ,name ,super-protocols ())
     ,@body
     (setf (find-protocol ',name)
           (make-instance 'protocol
                          :name ',name
                          :items
                          (list ,@(mapcar #'transform-place-description places)
                                ,@ (items<-define-protocol-body body))))))
