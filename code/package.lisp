#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:gnuxie.protocol
  (:use #:cl)
  (:local-nicknames
   (#:a #:alexandria))
  (:export
   #:find-protocol
   #:items
   #:name
   #:protocol

   #:protocol-item
   #:lambda-list

   #:initargs
   #:writers
   #:protocol-place

   #:define-protocol))

(in-package #:gnuxie.protocol)
