#|
    Copyright (C) 2021 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:gnuxie.protocol)

(defvar *protocols* (make-hash-table))

(defun find-protocol (name)
  (gethash name *protocols*))

(defun (setf find-protocol) (p name)
  (setf (gethash name *protocols*) p))

(defgeneric items (protocol))
(defclass protocol ()
  ((%name :initarg :name :reader name)
   (%items :initarg :items :initform '())))



(defgeneric protocol (protocol-item))
(defgeneric name (protocol-item))
(defclass protocol-item () ())

(defgeneric lambda-list (protocol-generic-function))
(defclass protocol-generic-function ()
  ((%generic-function-name :initarg :name :reader name)))

(defmethod lambda-list ((protocol-generic-function protocol-generic-function))
  (c2mop:generic-function-lambda-list
   (symbol-function (name protocol-generic-function))))

(defgeneric initargs (protocol-place))
(defgeneric writers (protocol-place))
(defclass protocol-place (protocol-generic-function)
  ((%writers :initarg :writers :reader writers :initform '())
   (%initargs :initarg :initargs :reader initargs :initform '())
   (%lambda-list :initarg :lambda-list :reader lambda-list
                 :initform (error "The lambda list to the reader of
the protocol place must be provided."))))
