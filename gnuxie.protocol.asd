(cl:in-package #:asdf-user)

(defsystem #:gnuxie.protocol
  :author "Gnuxie <gnuxie@applied-langua.ge>"
  :serial t
  :depends-on ("alexandria" "closer-mop")
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "protocol")
                         (:file "macros")))))
